// ==UserScript==
// @match *://*/*dashboard.action*
// ==/UserScript==
"use strict";
/*
 * Add delete link before the summary. 
 * Extension runs after document has been loaded completely, no need to wait for documentready
 */
jQuery('#recent-updates .update-item .update-item-content')
   .filter(':has(a[href*="#comment-"])') // only comments
   .find('span:last-of-type')  // after last span
   .after(' <span>(<a class="dashboard-update-item-delete">' + chrome.i18n.getMessage('delete') + '</a>)</span>');
jQuery('#recent-updates').delegate('.update-item:not(.removed) .dashboard-update-item-delete', 'click', function(e){
   var deleteLink = jQuery(this);
   var commentItem = deleteLink.closest('.update-item');
   var commentLink = commentItem.find('.update-item-title a');
   var commentUrl = commentLink.attr('href');
   
   // click twice to delete comment
   if( !deleteLink.hasClass('confirm') ){
      deleteLink.addClass('confirm');
      deleteLink.text( chrome.i18n.getMessage('confirm') );
   }else{
       // second click, delete comment
	   jQuery.get( commentUrl, function(data){
		  var deleteLinks = jQuery(data).find('.comment.focused .comment-action-remove a');
		  // be careful not to select more comments
		  if( deleteLinks.size() === 1 ){
			// delete comment. without the confirm=yes parameter confluence shows a confirmation page first
			jQuery.get( deleteLinks.attr('href')  + '&confirm=yes', function(){
				commentItem.addClass('removed');
				deleteLink.text( chrome.i18n.getMessage('deleted') );
			});
		  }else{
		  	// no link found (no comment on page or no permissions to remove)
		  	// remove delete link from that comment
		  	deleteLink.parent('span').remove();
		  }
	   }, 'html' );
   }
})
